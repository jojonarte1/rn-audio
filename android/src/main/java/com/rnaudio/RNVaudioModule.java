
package com.rnaudio;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioDeviceInfo;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
// import android.support.annotation.Nullable;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
// import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import java.io.FileOutputStream;

public class AudioModule extends ReactContextBaseJavaModule {

  private static final int RECORDING_RATE = 44100;
  private static final int CHANNEL = AudioFormat.CHANNEL_IN_MONO;
  private static final int FORMAT = AudioFormat.ENCODING_PCM_16BIT;
  private AudioRecord recorder;
  private static int BUFFER_SIZE = AudioRecord.getMinBufferSize(RECORDING_RATE, CHANNEL, FORMAT);
  private boolean currentlySendingAudio = false;

  private MediaPlayer song;
  private MediaPlayer recordedSong;
  private MediaPlayer wordListAudio;
  private MediaPlayer autoPlayAudio;

  private HeadsetPlugReceiver headsetPlugReceiver;

  private class HeadsetPlugReceiver extends BroadcastReceiver {
    public static final int NO_HEAD_PHONE = 0;
    public static final int HEAD_PHONE_ONLY = 1;
    public static final int HEAD_PHONE_AND_MIC = 2;

    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
        boolean connectedHeadphones = intent.getIntExtra("state", 0) == 1;
        boolean connectedMicrophone = intent.getIntExtra("microphone", 0) == 1;

        if (connectedHeadphones && connectedMicrophone) {
          WritableMap params = Arguments.createMap();
          params.putInt("headphonemicstate", HEAD_PHONE_AND_MIC);
          sendEvent("headphonemicstatechange", params);
        } else if (connectedHeadphones) {
          WritableMap params = Arguments.createMap();
          params.putInt("headphonemicstate", HEAD_PHONE_ONLY);
          sendEvent("headphonemicstatechange", params);
        } else {
          WritableMap params = Arguments.createMap();
          params.putInt("headphonemicstate", NO_HEAD_PHONE);
          sendEvent("headphonemicstatechange", params);
        }
      }
    }
  }

  public AudioModule(ReactApplicationContext reactContext) {
    super(reactContext); // required by React Native
  }

  private void startRecording(final Promise promise) {
    Thread streamThread = new Thread(new Runnable() {

      @Override
      public void run() {
        try {
          song.start();
          byte[] buffer = new byte[BUFFER_SIZE];

          recorder = new AudioRecord(MediaRecorder.AudioSource.MIC, RECORDING_RATE, CHANNEL, FORMAT,
                  BUFFER_SIZE * 10);

          recorder.startRecording();
          // String filePath = Environment.getExternalStorageDirectory().getAbsolutePath()
          // + "/voice8K16bitmono.pcm";
          String filePath = getReactApplicationContext().getCacheDir().getAbsolutePath()
                  + "/voice8K16bitmono.pcm";
          FileOutputStream os = new FileOutputStream(filePath);

          while (currentlySendingAudio == true) {

            recorder.read(buffer, 0, buffer.length);

            os.write(buffer, 0, buffer.length);
          }

          os.close();
          recorder.release();
          promise.resolve("");

        } catch (Exception e) {
          Log.e("TAG#####1", "Exception: " + e);
        }
      }
    });

    streamThread.start();
  }

  @Override
  public String getName() {
    return "Audio"; // Audio is how this module will be referred to from React Native
  }

  @ReactMethod
  public void sayHello(Promise promise) { // this method will be called from JS by React Native
    promise.resolve("Hello from Android");
  }

  @ReactMethod
  public void setup(String audioPath, Promise promise) {
    // Uri uri =
    // Uri.parse(Environment.getExternalStorageDirectory().getAbsolutePath() +
    // "/day1.wav");
    Uri uri = Uri.parse(audioPath);

    song = MediaPlayer.create(getReactApplicationContext(), uri);

    song.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      @Override
      public void onCompletion(MediaPlayer mp) {
        currentlySendingAudio = false;
      }
    });

    this.listenNetworkInfoChange(promise);
  }

  @ReactMethod
  public void releaseResources() {
    if (song != null) {
      song.stop();
      song.release();
    }

    if (recorder != null) {
      recorder.release();
    }

    getReactApplicationContext().unregisterReceiver(headsetPlugReceiver);
  }

  @ReactMethod
  public void startStreamingAudio(Promise promise) {
    song.start();
    currentlySendingAudio = true;
    startRecording(promise);
  }

  @ReactMethod
  public void stopStreamingAudio() {
    currentlySendingAudio = false;
    if (recorder != null) {
      recorder.stop();
    }
    song.pause();
    song.seekTo(0);
  }

  @ReactMethod
  public void headphonesPluggedIn(Promise promise) {

    AudioManager audioManager = (AudioManager) getReactApplicationContext().getSystemService(Context.AUDIO_SERVICE);
    AudioDeviceInfo[] audioDevices = audioManager.getDevices(AudioManager.GET_DEVICES_ALL);
    for (AudioDeviceInfo deviceInfo : audioDevices) {
      if (deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADPHONES
              || deviceInfo.getType() == AudioDeviceInfo.TYPE_WIRED_HEADSET) {
        promise.resolve(true);
        return;
      }
    }
    promise.resolve(false);
  }

  @ReactMethod
  public void mergeAudio() {

  }

  @ReactMethod
  public void playRecordedAudio(final Promise promise) {
    // Uri uri =
    // Uri.parse(Environment.getExternalStorageDirectory().getAbsolutePath() +
    // "/out.m4a");
    Uri uri = Uri.parse(getReactApplicationContext().getFilesDir().getAbsolutePath() + "/out.m4a");

    recordedSong = MediaPlayer.create(getReactApplicationContext(), uri);
    recordedSong.start();
    recordedSong.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      public void onCompletion(MediaPlayer recordedSong) {
        recordedSong.release();
        promise.resolve("");
      }
    });
  }

  @ReactMethod
  public void stopPlayingRecordedAudio() {
    if (recordedSong != null) {
      recordedSong.stop();
      recordedSong.release();
    }
  }

  @ReactMethod
  public void refreshGallery(String imagePath) {
    Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
    intent.setData(Uri.parse("file://" + imagePath));
    getReactApplicationContext().sendBroadcast(intent);
  }

  @ReactMethod
  public void playWordListAudio(String audioPath, final Promise promise) {
    // Uri uri = Uri.parse(getReactApplicationContext().getFilesDir().getAbsolutePath() + audioPath);
    Uri uri = Uri.parse(audioPath);
    wordListAudio = MediaPlayer.create(getReactApplicationContext(), uri);
    wordListAudio.start();
    wordListAudio.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      public void onCompletion(MediaPlayer wordListAudio) {
        wordListAudio.release();
        wordListAudio = null;
        promise.resolve("");
      }
    });
  }

  @ReactMethod
  public void stopWordListAudio() {
    if (wordListAudio != null) {
      wordListAudio.release();
    }
  }

  @ReactMethod
  public void setupAutoPlayAudio(String audioPath) {
    Uri uri = Uri.parse(getReactApplicationContext().getFilesDir().getAbsolutePath() + audioPath);
    autoPlayAudio = MediaPlayer.create(getReactApplicationContext(), uri);
  }

  @ReactMethod
  public void playAutoPlayAudio(final Promise promise) {
    autoPlayAudio.start();
    autoPlayAudio.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
      public void onCompletion(MediaPlayer wordListAudio) {
        autoPlayAudio.release();
        autoPlayAudio = null;
        promise.resolve("");
      }
    });
  }

  @ReactMethod
  private void pauseAutoPlayAudio() {
    autoPlayAudio.pause();
  }

  @ReactMethod
  public void stopAutoPlayAudio() {
    if (autoPlayAudio != null) {
      autoPlayAudio.release();
    }
  }

  // private void sendEvent(String eventName, @Nullable WritableMap params) {
  private void sendEvent(String eventName, WritableMap params) {
    getReactApplicationContext().getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class).emit(eventName,
            params);
  }

  private void listenNetworkInfoChange(Promise promise) {
    headsetPlugReceiver = new HeadsetPlugReceiver();

    IntentFilter intentFilter = new IntentFilter("android.intent.action.HEADSET_PLUG");
    getReactApplicationContext().registerReceiver(headsetPlugReceiver, intentFilter);
    promise.resolve("");
  }

}