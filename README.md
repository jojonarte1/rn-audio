# react-native-vaudio

## Getting started

`$ npm install git+ssh://git@bitbucket.org:jojonarte1/rn-audio.git --save`

### Mostly automatic installation

`$ react-native link rn-audio`

### Manual installation

#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-vaudio` and add `RNVaudio.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNVaudio.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`

- Add `import com.reactlibrary.RNVaudioPackage;` to the imports at the top of the file
- Add `new RNVaudioPackage()` to the list returned by the `getPackages()` method

2. Append the following lines to `android/settings.gradle`:
   ```
   include ':react-native-vaudio'
   project(':react-native-vaudio').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-vaudio/android')
   ```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
   ```
     compile project(':react-native-vaudio')
   ```

#### Windows

[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNVaudio.sln` in `node_modules/react-native-vaudio/windows/RNVaudio.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app

- Add `using Vaudio.RNVaudio;` to the usings at the top of the file
- Add `new RNVaudioPackage()` to the `List<IReactPackage>` returned by the `Packages` method

## Usage

```javascript
import RNVaudio from 'react-native-vaudio';

// inside React.componentDidMount for iOS setup has to be called.

class Sample extends React.Component {
	componentDidMount() {
		if (Platform.OS === 'ios') {
			AudioModule.setup();
		}
	}

	render() {
		/***render implementation**/
	}
}
```

### Setting up (TODO)

for ios
**askPermission**
for android use

```
PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
      ])
```

### Functions (TODO)

**setup(audioPath) -> Promise**

Setup file for playback.
on Android it accepts path to where an audiopath is in the file system.

**start**
(confusing see Kikutan `RecordChantStartScreen.js`)

**stop**
(confusing see Kikutan `RecordChantStartScreen.js`)

**setupAutoPlayAudio(audioPath)**
setup audio to auto play

**stopAutoPlayAudio()**
stops auto playing audio

**pauseAutoPlayAudio()**
pauses auto playing audio

**playAutoPlayAudio()**
starts the autoplay audio

**releaseResources()**
releases resources used by the library (advisable to call whenever unmounting)

**stopPlayingRecordedAudio()**
stop playing the latest recorded audio

**playRecordedAudio**
plays the latest recorded audio

**refreshGallery**
(TODO)

**startStreamingAudio**

- plays the audio passed in setup
- records audio input

**stopStreamingAudio**

resets audio position to 0 and stops audio stream and recording

**mergeAudio**

(empty)

**createVideo**

(empty)

**playWordListAudio**
play word list

**stopWordListAudio**
stop wordlist
