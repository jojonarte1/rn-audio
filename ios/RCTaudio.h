
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

#import <React/RCTEventEmitter.h>

@interface RCTAudio : RCTEventEmitter <RCTBridgeModule, AVAudioRecorderDelegate, AVAudioPlayerDelegate>

@property AVAudioPlayer *player;

@property AVAudioSession *recordingSession;

@property AVAudioRecorder *audioRecorder;

@property (nonatomic, assign) BOOL initialHeadphoneStateWasSet;

@property (nonatomic, assign) BOOL recordingWasCancelled;

@property (nonatomic) NSMutableString* minusOneAudioPath;

@property (nonatomic) NSMutableString* videoImagePath;

@property (nonatomic, assign) int day;

@property (nonatomic, assign) NSString* course;

@end

