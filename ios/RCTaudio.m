
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreImage/CoreImage.h>
#import <Photos/Photos.h>
#import "RCTAudio.h"

const int NO_HEAD_PHONE = 0;
const int HEAD_PHONE_ONLY = 1;
const int HEAD_PHONE_AND_MIC = 2;
const int HEAD_PHONE = 3;

@implementation RCTAudio

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.initialHeadphoneStateWasSet = false;
    self.recordingWasCancelled = false;
    self.day = 1;
    self.minusOneAudioPath = [[NSMutableString alloc] initWithString:@"chant-basic-chant-day-1-recordingAudioUrl"];
    self.videoImagePath = [[NSMutableString alloc] initWithString:@""];
    self.course = @"basic";
  }
  return self;
}

RCT_EXPORT_MODULE();

- (NSArray<NSString *> *)supportedEvents {
  return @[@"headphonemicstatechange", @"videoencoding", @"finishedaudio"];
}

- (void) playAudio:(NSString *) path {
  // NSURL *url = [[NSBundle mainBundle] URLForResource:@"sample_1" withExtension:@"m4a"];
  // NSURL *url = [NSURL fileURLWithPath:path];
  NSURL *url = [[self getDocumentsDirectory] URLByAppendingPathComponent: path];
  if (url != NULL) {
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"m4a" error:nil];
    [_player setDelegate:self];
    
    [_player play];
  } else {
    NSLog(@"playAudio: audio file not found");
  }
}

- (void)finishRecording:(BOOL)success {
  [_audioRecorder stop];
  _audioRecorder = nil;
  
  if (success) {
    NSLog(@"recorded");
  } else {
    NSLog(@"not recorded");
  }
}

- (void) stopAudio {
  [_player stop];
  [self finishRecording:true];
}

- (NSURL *) getDocumentsDirectory {
  return [NSURL fileURLWithPath:[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]];
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag {
  if (!flag) {
    [self finishRecording:false];
  } else {
    if (!self.recordingWasCancelled) {
      NSLog(@"Path Now: %@", self.minusOneAudioPath);
//      [self mergeAudio:self.minusOneAudioPath];
      [self createVideo: self.videoImagePath];
    }
  }
  
  NSLog(@"#### audioRecorderDidFinishRecording: %s", flag ? "YES" : "NO");
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
  [self finishRecording:YES];
  [self sendEventWithName:@"finishedaudio" body:@{@"status": @"FINISHED PLAYING AUDIO"}];
}

- (void) startRecording {
  NSURL *audioFilename = [[self getDocumentsDirectory] URLByAppendingPathComponent:@"recording.wav"];
  
  NSDictionary *settings = [NSDictionary
                                  dictionaryWithObjectsAndKeys:
                                  [NSNumber numberWithInt:kAudioFormatLinearPCM],
                                  AVFormatIDKey,
                                  [NSNumber numberWithInt:AVAudioQualityHigh],
                                  AVEncoderAudioQualityKey,
                                  [NSNumber numberWithInt: 1],
                                  AVNumberOfChannelsKey,
                                  [NSNumber numberWithFloat:12000],
                                  AVSampleRateKey,
                                  nil];
  @try {
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:audioFilename settings:settings error: nil];
    [_audioRecorder setDelegate:self];
    [_audioRecorder record];
  }
  @catch (NSException *exception) {
    NSLog(@"### __ %@", exception.reason);
  }
}

RCT_REMAP_METHOD(sayHello,
                 say_hello_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  resolve(@"Hello from Objective-C");
}

RCT_REMAP_METHOD(headphonesPluggedIn,
                 head_phones_pluggedin_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject)
{
  AVAudioSessionRouteDescription *currentRoute = [[AVAudioSession sharedInstance] currentRoute];
  if (currentRoute.outputs.count != 0) {
    AVAudioSessionPortDescription *description = [currentRoute.outputs objectAtIndex:0];
    
    if ([description.portType isEqualToString:AVAudioSessionPortHeadphones]) {
      resolve(@YES);
    }
    else {
      resolve(@NO);
    }
  }
  else {
    resolve(@NO);
  }
}

- (void) audioRouteChangeListener:(NSNotification *)notif {
  NSDictionary *userInfo = [notif userInfo];
  AVAudioSessionRouteDescription *audioRouteChangeReason = userInfo[AVAudioSessionRouteChangePreviousRouteKey];
  AVAudioSessionPort port = [[audioRouteChangeReason.outputs objectAtIndex:0] portType];
  
  if (self.initialHeadphoneStateWasSet) {
    if ([port isEqualToString: AVAudioSessionPortHeadphones]) {
      NSLog(@"### headphone plugged out");
      [self sendEventWithName:@"headphonemicstatechange" body:@{@"headphonemicstate": [NSNumber numberWithInt:NO_HEAD_PHONE]}];
      //    } else if ([port isEqualToString: AVAudioSessionPortHeadsetMic]) {
    } else {
      NSLog(@"### headphone plugged in");
      [self sendEventWithName:@"headphonemicstatechange" body:@{@"headphonemicstate": [NSNumber numberWithInt:HEAD_PHONE]}];
    }
  } else {
    AVAudioSessionRouteDescription *currentRoute = [[AVAudioSession sharedInstance] currentRoute];
    if (currentRoute.outputs.count != 0) {
      for(AVAudioSessionPortDescription *description in currentRoute.outputs) {
        if ([description.portType isEqualToString:AVAudioSessionPortHeadphones]) {
          NSLog(@"### headphone plugged in");
          [self sendEventWithName:@"headphonemicstatechange" body:@{@"headphonemicstate": [NSNumber numberWithInt:HEAD_PHONE]}];
        }
      }
    }

    self.initialHeadphoneStateWasSet = true;
  }
}

- (void) makeMovieFromImageData:(UIImage *)image :(NSTimeInterval)duration :(void (^)(void))completion {
  NSError *error;
  
  // width has to be divisible by 16 or the movie comes out distorted... don't ask me why
  double pixelsToRemove = fmod(image.size.width, 16);
  
  double pixelsToAdd = 16 - pixelsToRemove;
  
  CGSize size = CGSizeMake(image.size.width+pixelsToAdd, image.size.height);
  

  NSURL *tempFileURL;
  NSString *outputFile;
  
  outputFile = [NSString stringWithFormat:@"movie.mp4"];
  
  //    NSString *outputDirectory = [self getDocumentsDirectory];//[NSSearchPathForDirectoriesInDomains(NSTemporaryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
  
  tempFileURL = [[self getDocumentsDirectory] URLByAppendingPathComponent:outputFile];
  
  // Start writing
  AVAssetWriter *videoWriter = [[AVAssetWriter alloc] initWithURL:tempFileURL
                                                         fileType:AVFileTypeQuickTimeMovie
                                                            error:&error];
  
  if (error) {
    // handle error
  }
  
  NSDictionary *videoSettings = [NSDictionary dictionaryWithObjectsAndKeys:
                                 AVVideoCodecH264, AVVideoCodecKey,
                                 [NSNumber numberWithInt:size.width], AVVideoWidthKey,
                                 [NSNumber numberWithInt:size.height], AVVideoHeightKey,
                                 nil];
  
  AVAssetWriterInput* writerInput = [AVAssetWriterInput assetWriterInputWithMediaType:AVMediaTypeVideo
                                                                       outputSettings:videoSettings];
  
  NSDictionary *bufferAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                    [NSNumber numberWithInt:kCVPixelFormatType_32ARGB], kCVPixelBufferPixelFormatTypeKey, nil];
  
  AVAssetWriterInputPixelBufferAdaptor *adaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:writerInput
                                                                                                                   sourcePixelBufferAttributes:bufferAttributes];
  if ([videoWriter canAddInput:writerInput]) {
    [videoWriter addInput:writerInput];
  } else {
    // handle error
  }
  
  [videoWriter startWriting];
  
  [videoWriter startSessionAtSourceTime:kCMTimeZero];
  
//  CGImageRef img = [image CGImage];
  
  // Now I am going to create the bixelBuffer
  NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                           [NSNumber numberWithBool:YES], kCVPixelBufferCGImageCompatibilityKey,
                           [NSNumber numberWithBool:YES], kCVPixelBufferCGBitmapContextCompatibilityKey,
                           nil];
  CVPixelBufferRef buffer = NULL;
  
  CVReturn status = CVPixelBufferCreate(kCFAllocatorDefault, size.width,
                                        size.height, kCVPixelFormatType_32ARGB, (__bridge CFDictionaryRef) options,
                                        &buffer);
  
  if ( !(status == kCVReturnSuccess && buffer != NULL) ) {
    NSLog(@"There be some issue. We didn't get a buffer from the image");
  }
  
  
  CVPixelBufferLockBaseAddress(buffer, 0);
  void *pxdata = CVPixelBufferGetBaseAddress(buffer);
  
  CGColorSpaceRef rgbColorSpace = CGColorSpaceCreateDeviceRGB();
  
  CGContextRef context = CGBitmapContextCreate(pxdata, size.width,
                                               size.height, 8, 4*size.width, rgbColorSpace,
                                               (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
  CGContextSetRGBFillColor(context, 0, 0, 0, 0);
  
  CGContextConcatCTM(context, CGAffineTransformIdentity);
  
  CGContextDrawImage(context, CGRectMake(0, 0, size.width,
                                         size.height), [image CGImage]);
  CGColorSpaceRelease(rgbColorSpace);
  CGContextRelease(context);
  
  CVPixelBufferUnlockBaseAddress(buffer, 0);
  
  // At this point we have our buffer so we are going to start by adding to time zero
  
  [adaptor appendPixelBuffer:buffer withPresentationTime:kCMTimeZero];
  
  while (!writerInput.readyForMoreMediaData) {} // wait until ready
  
  CMTime frameTime = CMTimeMake(duration, 1); // 5 second frame
  
  [adaptor appendPixelBuffer:buffer withPresentationTime:frameTime];
  CFRelease(buffer);
  
  [writerInput markAsFinished];
  
  [videoWriter endSessionAtSourceTime:frameTime];
  
  [videoWriter finishWritingWithCompletionHandler:^{
    completion();
    if (videoWriter.status != AVAssetWriterStatusCompleted) {
      // Error
    }
  }]; // end videoWriter finishWriting Block
}

- (void) addAudioToMovie: (AVURLAsset *)audioAsset :(AVURLAsset *)inputVideoAsset :(NSURL *)outputVideoFileURL :(NSString *)quality {
  AVMutableComposition *composition = [[AVMutableComposition alloc] init];
  AVAssetTrack *videoAssetTrack = [[inputVideoAsset tracksWithMediaType:AVMediaTypeVideo] firstObject];
  NSParameterAssert(videoAssetTrack);
  AVMutableCompositionTrack *compositionVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];

  [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [inputVideoAsset duration]) ofTrack:videoAssetTrack atTime:kCMTimeZero error:nil];

  AVAssetTrack *audioAssetTrack = [[audioAsset tracksWithMediaType:AVMediaTypeAudio] firstObject];
  NSParameterAssert(audioAssetTrack);
  AVMutableCompositionTrack *compositionAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
  
  [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [audioAsset duration]) ofTrack:audioAssetTrack atTime:kCMTimeZero error:nil];
  AVAssetExportSession *assetExport = [[AVAssetExportSession alloc] initWithAsset:composition presetName:quality];
  assetExport.outputFileType = AVFileTypeQuickTimeMovie;
  assetExport.outputURL = outputVideoFileURL;
  [assetExport exportAsynchronouslyWithCompletionHandler:^{
    NSLog(@"### Movie Completed Sucessfully");
    [self sendEventWithName:@"videoencoding" body:@{@"status": @"Video Created"}];
  }];
}

- (void) createVideo:(NSString *)videoPath {
//    NSURL *audioFilename = [[self getDocumentsDirectory] URLByAppendingPathComponent:@"recording.wav"];
   AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:[[self getDocumentsDirectory] URLByAppendingPathComponent:@"recording.wav"] options:nil];
//  AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:[[self getDocumentsDirectory] URLByAppendingPathComponent:@"mixedAudio.m4a"] options:nil];
  NSTimeInterval length = CMTimeGetSeconds([audioAsset duration]);
  
  NSURL *url2 = [[self getDocumentsDirectory] URLByAppendingPathComponent: videoPath];

  UIImage *videoImage = [UIImage imageWithContentsOfFile: [url2 path]];
  
  [self makeMovieFromImageData: videoImage :length :^{
    AVURLAsset *videoAsset = [[AVURLAsset alloc] initWithURL:[[self getDocumentsDirectory] URLByAppendingPathComponent:@"movie.mp4"] options:nil];
    [self addAudioToMovie:audioAsset :videoAsset :[[self getDocumentsDirectory] URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%@%@%d%@", @"mymovie_", self.course, @"_Day_", self.day, @".mp4"]] :AVAssetExportPresetMediumQuality];
  }];
}

RCT_REMAP_METHOD(setup,
                 setup_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  @try {
    _recordingSession = [AVAudioSession sharedInstance];
    [_recordingSession setCategory:AVAudioSessionCategoryPlayAndRecord mode:AVAudioSessionModeVideoRecording options:AVAudioSessionCategoryOptionMixWithOthers error: nil];
    [_recordingSession setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [_recordingSession setMode:AVAudioSessionModeVideoRecording error:nil];
    [_recordingSession setActive:true error: nil];
    [_recordingSession requestRecordPermission:^(BOOL granted) {
      dispatch_async(dispatch_get_main_queue(), ^{
        if (granted) {
          NSLog(@"### %@", @"allowed");
        } else {
          NSLog(@"### %@", @"not allowed");
        }
      });
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioRouteChangeListener:) name:AVAudioSessionRouteChangeNotification object:nil];
    resolve(@"");
  }
  @catch (NSException *exception) {
    NSLog(@"### %@", exception.reason);
  }
}

RCT_REMAP_METHOD(releaseResources,
                 resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  NSLog(@"### %@", @"no implementation yet");
}

RCT_REMAP_METHOD(start,
                 audioPath: (NSString *) path
                 thumbnailPath: (NSString *) path2
                 start_streaming_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  self.recordingWasCancelled = false;
  [self.minusOneAudioPath setString:path];
  [self.videoImagePath setString:path2];
  [self playAudio:path];
  [self startRecording];
}

RCT_REMAP_METHOD(stop,
                 :(BOOL) cancelled
                 day: (int) day
                 course: (NSString *) course
                 audioPath: (NSString *) path
                 stop_streaming_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  self.day = day;
  [self.minusOneAudioPath setString:path];
  self.recordingWasCancelled = cancelled;
  self.course = course;
  [self stopAudio];
  [self finishRecording:YES];
}

RCT_REMAP_METHOD(playRecordedAudio,
                 play_recorded_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  NSURL *url = [[self getDocumentsDirectory] URLByAppendingPathComponent:@"recording.wav"];
  
  if (url != NULL) {
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"m4a" error:nil];
    [_player setDelegate:self];
    
    [_player play];
  } else {
    NSLog(@"playAudio: audio file not found");
  }
}

RCT_REMAP_METHOD(playWordListAudio,
                 audioPath: (NSString *) path
                 play_recorded_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  NSURL *url = [[self getDocumentsDirectory] URLByAppendingPathComponent: path];
  
  if (url != NULL) {
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"m4a" error:nil];
    [_player setDelegate:self];
    
    [_player play];
  } else {
    NSLog(@"playAudio: audio file not found");
  }
}

RCT_REMAP_METHOD(stopWordListAudio,
                 stop_wordlist_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  [_player stop];
}

RCT_REMAP_METHOD(stopPlayingRecordedAudio,
                 stop_recorded_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  [_player stop];
}

RCT_REMAP_METHOD(setupAutoPlayAudio,
                 audioPath: (NSString *) path
                 play_auto_play_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {

  NSURL *url = [[self getDocumentsDirectory] URLByAppendingPathComponent: path];
  
  if (url != NULL) {
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url fileTypeHint:@"m4a" error:nil];
    [_player setDelegate:self];
  } else {
    NSLog(@"playAudio: audio file not found");
  }
}

RCT_REMAP_METHOD(stopAutoPlayAudio,
                 stop_auto_play_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  [_player stop];
}

RCT_REMAP_METHOD(pauseAutoPlayAudio,
                 pause_auto_play_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  [_player pause];
}

RCT_REMAP_METHOD(playAutoPlayAudio,
                 play_auto_play_audio_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  [_player play];
}

RCT_REMAP_METHOD(refreshGallery,
                 audioPath: (NSString *) path
                 refresh_gallery_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  
  NSURL *url = [[self getDocumentsDirectory] URLByAppendingPathComponent: path];
  
  [[PHPhotoLibrary sharedPhotoLibrary] performChanges:^{
    [PHAssetChangeRequest creationRequestForAssetFromVideoAtFileURL: url];
  }
    completionHandler:^(BOOL success, NSError *error) {
      if (success)
      {
        resolve(@"");
        NSLog(@"Video saved");
      }else{
        reject(@"camera_roll_failed", [url path], error);
        NSLog(@"%@",error.description);
      }
    }];
}

RCT_REMAP_METHOD(askPermission,
                 permission_resolver:(RCTPromiseResolveBlock)resolve
                 rejecter:(RCTPromiseRejectBlock)reject) {
  @try {
    [_recordingSession requestRecordPermission:^(BOOL granted) {
      dispatch_async(dispatch_get_main_queue(), ^{
        if (granted) {
          resolve(@"Record permission granted.");
        } else {
          NSError* err = [NSError errorWithDomain:@"record" code:404 userInfo:nil];
          reject(@"reject_permission", @"Record permission denied.", err);
        }
      });
    }];
  }
  @catch (NSException *exception) {
    NSLog(@"### %@", exception.reason);
  }
}

- (void) mergeAudio: (NSString *)minusOnePath {
  AVMutableComposition *composition = [[AVMutableComposition alloc] init];
  NSURL *minusOneFile = [[self getDocumentsDirectory] URLByAppendingPathComponent:minusOnePath];
  
  AVURLAsset *audioAsset = [[AVURLAsset alloc] initWithURL:minusOneFile options:nil];

  AVMutableCompositionTrack *audioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
  
  @try {
    [audioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, [audioAsset duration]) ofTrack:[[audioAsset tracks] objectAtIndex:0] atTime:kCMTimeZero error:nil];

    NSURL *audioFilename = [[self getDocumentsDirectory] URLByAppendingPathComponent:@"recording.wav"];
    
    AVURLAsset *audioAsset2 = [[AVURLAsset alloc] initWithURL:audioFilename options:nil];

    AVMutableCompositionTrack *audioTrack2 = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    [audioTrack2 insertTimeRange:CMTimeRangeMake(kCMTimeZero, [audioAsset2 duration]) ofTrack:[[audioAsset2 tracks] objectAtIndex:0] atTime:[audioAsset duration] error:nil];
    
    AVAssetExportSession *assetExport = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetAppleM4A];
    
    NSURL *exportURL = [[self getDocumentsDirectory] URLByAppendingPathComponent:@"mixedAudio.m4a"];
    
    assetExport.outputFileType = AVFileTypeAppleM4A;
    assetExport.outputURL = exportURL;
    assetExport.shouldOptimizeForNetworkUse = true;
    [assetExport exportAsynchronouslyWithCompletionHandler:^{
      [self createVideo: self.videoImagePath];
    }];
  }
  @catch (NSException *exception) {
    NSLog(@"### %@", exception.reason);
    [self sendEventWithName:@"videoencoding" body:@{@"status": @"Failed"}];
  }
}

@end
